var menuItem = {
    "id" : "wikit",
    "title" : "wikit",
    "contexts" : ["selection"]
};

chrome.contextMenus.create(menuItem);

function fixedEncodeURI(str){
    return encodeURI(str).replace(/%58/g, '[').replace(/%50/g, ']');
}

chrome.contextMenus.onClicked.addListener(function(clickData){
    if(clickData.menuItemId === "wikit" && clickData.selectionText){
        var wikiUrl = "https://en.wikipedia.org/wiki/" + fixedEncodeURI(clickData.selectionText);
        var createData = {
            "url" : wikiUrl,
            "type" : "popup",
            "top" : 5,
            "left" : 5,
            "width" : parseInt(screen.availWidth/2),
            "height" : parseInt(screen.availHeight/2)
        };
        chrome.windows.create(createData, function(){});
    }
})