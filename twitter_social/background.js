var contextsList = ["selection", "link", "image", "page"];

for(i = 0; i < contextsList.length; i++){
    var context = contextsList[i];
    var titleX = "Twitter Toolkit: share this "+context+" on your twitter profile";
    chrome.contextMenus.create({
        title : titleX,
        contexts:[context],
        onclick: clickHandler,
        id: context
    })
}
//selection, link, image, page
function clickHandler(data, tab){
    switch(data.menuItemId){
        case 'selection' :
            chrome.windows.create({url: "https://twitter.com/intent/tweet?text=" + encodeURIComponent(data.selectionText)
                                    , type: "panel", width:600, height:600})
            break;
        case 'link':
            chrome.windows.create({url: "https://twitter.com/intent/tweet?url=" + encodeURIComponent(data.linkUrl)
                                    , type: "panel", width:600, height:600})
            break;
        case 'image':
            chrome.windows.create({url: "https://twitter.com/intent/tweet?url=" + encodeURIComponent(data.srcUrl)
                                    , type: "panel", width:600, height:600})
            break;
        case 'page':
            chrome.windows.create({url: "https://twitter.com/intent/tweet?text="+ encodeURIComponent(tab.title) + "&url=" + (data.pageUrl)
                                     , type: "panel", width:600, height:600})
            break;
              
    }
}